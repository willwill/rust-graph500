# Graph500 in Rust
Only tested with nightly Rust, and probably won't work on stable as we use [serde](https://serde.rs/).

## Building
1. Run `cargo build --release`
2. The resulting binaries will be in `target/release/`

## Running
### Generator
The generator binary is named `generator`. To view usage, invoke with `-h`.

Typical usage: `./generator -o out.txt 19`. This will generate and save
a graph with SCALE=19. `--edgefactor` can also be specified.

### Graph500
This will load and run the actual Graph500.

Typical usage:

- Master: `./graph500 --master 3 out.txt`: Run with cluster size of 3 (including self)
- Slave:  `./graph500 --slave 127.0.0.1 out.txt`: Run with cluster size of 3 (including self)
- `-r` can be used to specify NBFS on the master node.
- Slave is recommended to run first, as it would try several times to connect to master.

## Internal
### File format
The output file is generated from [bincode](https://github.com/TyOverby/bincode).

The format contains the [graph500::config::Config](src/config.rs) struct followed by
several [graph500::generator::Edge](src/generator.rs) structs.

### Kernel 1 format
Kernel 1 will iterate the input file and load the graph as a vector of nodes.

Each node is a struct that stores vector of edges. Edge is a struct storing the
destination edge and weight.
