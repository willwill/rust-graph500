use std::io::prelude::*;
use loader::Loader;
use graph::*;

pub fn load<T: Read + Seek>(loader: &mut Loader<T>, start_index: NodeIndex, end_index: NodeIndex) -> Graph<Node> {
    let mut graph = Graph::new(Node::new(), (end_index - start_index) as usize, start_index);

    for edge in loader {
        if graph.is_valid(edge.start) {
            graph.add_edge(edge.start, edge.end, Weight::default());
        }

        if graph.is_valid(edge.end) {
            graph.add_edge(edge.end, edge.start, Weight::default());
        }
    }

    graph
}
