extern crate graph500;
extern crate clap;
extern crate bincode;

use std::fs::File;
use std::time::Instant;
use clap::{App, Arg};
use graph500::*;

fn main(){
    let cli = App::new("Edge list generation")
        .about("Conformant to Graph500 v1.2 section 3")
        .arg(Arg::with_name("scale")
            .help("The logarithm base two of the number of vertices")
            .required(true)
            .index(1))
        .arg(Arg::with_name("edgefactor")
            .long("edgefactor")
            .default_value("16")
            .help("The ratio of the graph's edge count to its vertex count (i.e., half the average degree of a vertex in the graph)")
            .short("e"))
        .arg(Arg::with_name("output")
            .help("Output file name")
            .short("o")
            .long("output")
            .takes_value(true)
            .required(true))
        .get_matches();

    let mut config = Config::new(cli.value_of("scale").unwrap().parse().unwrap());

    match cli.value_of("edgefactor") {
        Some(x) => config.edgefactor = x.parse().unwrap(),
        None => {}
    }

    let mut file = File::create(cli.value_of("output").unwrap()).unwrap();

    let mut now = Instant::now();
	let edges = kronecker_generator(&config);
    println!("construction_time: {}", duration_to_f64(&now.elapsed()));

    now = Instant::now();
    bincode::serde::serialize_into(&mut file, &config, bincode::SizeLimit::Infinite).unwrap();

    for edge in edges {
        bincode::serde::serialize_into(&mut file, &edge, bincode::SizeLimit::Infinite).unwrap();
    }

    println!("save_time: {}", duration_to_f64(&now.elapsed()));
}
