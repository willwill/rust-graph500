use config::Config;
use rand::Rng;
use rand::StdRng;

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Edge {
	pub start: u64,
	pub end: u64,
}

pub fn kronecker_generator(config: &Config) -> Vec<Edge> {
	let mut edges: Vec<Edge> = Vec::with_capacity(config.m as usize);
	let mut random = StdRng::new().unwrap();

	// Loop over each order of bit.
	let ab = config.a + config.b;
	let a_norm = config.a/ab;
	let c_norm = config.c / (1f32 - ab);

	for _ in 0..config.m as usize {
		let mut start = 0u64;
		let mut end = 0u64;

		for ib in 0..config.scale {
			let coeff = 1 << ib; // pow(2, n)
			// Compare with probabilities and set bits of indices.
			let mut start2 = 0u64;
			let mut end2 = 0u64;

			if random.next_f32() > ab {
				start2 = 1u64;
			}

			if random.next_f32() > (c_norm * start2 as f32) + (a_norm * ((start2 + 1) % 2) as f32) {
				end2 = 1u64;
			}

			start = start + coeff * start2;
			end = end + coeff * end2;
		}

		edges.push(Edge {
			start: start,
			end: end,
		});
	}

	edges
}
