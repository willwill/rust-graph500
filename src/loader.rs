use std::io::prelude::*;
use std::io::SeekFrom;
use bincode;
use config::Config;
use generator::Edge;

pub struct Loader<T: Read + Seek> {
    file: T,
    pub config: Option<Config>,
}

impl<T: Read + Seek> Loader<T> {
    pub fn new(file: T) -> Loader<T> {
        let mut result = Loader{
            file: file,
            config: None,
        };
        result.read_config();
        result
    }

    fn read_config(&mut self) {
        self.file.seek(SeekFrom::Start(0)).unwrap();
        self.config = bincode::serde::deserialize_from(&mut self.file, bincode::SizeLimit::Infinite).ok();
    }
}

impl<T: Read + Seek> Iterator for Loader<T> {
    type Item = Edge;

    fn next(&mut self) -> Option<Edge> {
        bincode::serde::deserialize_from(&mut self.file, bincode::SizeLimit::Infinite).ok()
    }
}
