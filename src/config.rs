#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub scale: u32,
    pub edgefactor: u32,
    pub n: u64,
    pub m: u64,
    pub a: f32,
    pub b: f32,
    pub c: f32,
    pub d: f32,
}

impl Config {
    pub fn new(scale: u32) -> Config {
        let mut result = Config {
            scale: scale,
            edgefactor: 16,
            n: 0,
            m: 0,

            a: 0.57,
            b: 0.19,
            c: 0.19,
            d: 0.05,
        };
        result.update_consts();
        result
    }

    pub fn get_vertices_count(&self) -> u64 {
        2u64.pow(self.scale)
    }

    pub fn get_edges_count(&self) -> u64 {
        self.edgefactor as u64 * self.get_vertices_count()
    }

    pub fn update_consts(&mut self) {
        self.n = self.get_vertices_count();
        self.m = self.get_edges_count();
    }

    pub fn print(&self) {
        println!("SCALE: {}\nedgefactor: {}", self.scale, self.edgefactor);
    }
}
