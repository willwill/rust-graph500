use std::sync::mpsc::Receiver;

pub trait MakeIterPeekable<T>{
	fn iter_peekable(&self) -> IterPeekable<T>;
}

impl<T> MakeIterPeekable<T> for Receiver<T> {
	fn iter_peekable(&self) -> IterPeekable<T> {
		IterPeekable {
			receiver: self,
			next: None,
		}
	}
}

pub struct IterPeekable<'a, T: 'a>{
	receiver: &'a Receiver<T>,
	next: Option<T>,
}

impl<'a, T> IterPeekable<'a, T> {
	pub fn peek(&mut self) -> Option<&T> {
		if self.next.is_some() {
			return self.next.as_ref();
		}

		let next = self.receiver.try_recv();
		match next {
			Ok(item) => {
				self.next = Some(item);
				self.next.as_ref()
			}
			_ => None
		}
	}
}

impl<'a, T> Iterator for IterPeekable<'a, T>{
	type Item = T;

	fn next(&mut self) -> Option<T> {
		match self.next.take() {
			Some(item) => {
				self.next = None;
				Some(item)
			},
			None => {
				self.receiver.recv().ok()
			}
		}
	}
}
