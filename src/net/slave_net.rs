use std::net::TcpStream;
use std::sync::mpsc::{Sender, Receiver};
use std::thread;
use bincode;
use net::messages::*;
use net::read_packet;

pub struct SlaveNet {
	network: TcpStream,
}

impl SlaveNet {
	pub fn new(addr: (&str, u16)) -> ::std::io::Result<SlaveNet> {
		Ok(SlaveNet {
			network: try!(TcpStream::connect(addr)),
		})
	}

	pub fn bind_slave(&mut self, slave: (Sender<NetworkMessage>, Receiver<NetworkMessage>)) {
		let receiver = slave.1;
		let mut network_clone = self.network.try_clone().unwrap();
		thread::Builder::new().name("slave output".to_string()).spawn(move || {
			// got packet from slave, send to network
			loop {
				match receiver.recv() {
					Ok(packet) => {
						println!("<< {:?}", packet);
						bincode::serde::serialize_into(&mut network_clone, &packet, bincode::SizeLimit::Infinite).unwrap();
					},
					_ => break,
				}
			}
		}).unwrap();

		loop {
			// got packet from master, send to bus
			match read_packet(&mut self.network) {
				Ok(NetworkMessage::Stop) => {
					println!(">> Stop");
					slave.0.send(NetworkMessage::Stop).unwrap();
					return;
				},
				Ok(packet) => {
					println!(">> {:?}", packet);
					slave.0.send(packet).unwrap();
				},
				_ => {}
			}

		}
	}
}
