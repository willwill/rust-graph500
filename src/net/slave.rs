use std::io::prelude::*;
use std::sync::mpsc::Sender;
use std::rc::Rc;
use std::io::stderr;
use std::collections::VecDeque;
use loader::Loader;
use net::messages::*;
use net::ClientIndex;
use kernel1;
use graph::*;
use bfs::BfsKernel;

pub struct Slave<'a, T: Read + Seek + 'a>{
	loader: &'a mut Loader<T>,
	graph: Option<Rc<Graph<Node>>>,
	channel: Sender<NetworkMessage>,
	bfs_kernel: Option<BfsKernel>,
}

impl<'a, T: Read + Seek> Slave<'a, T> {
	pub fn new(loader: &'a mut Loader<T>, channel: Sender<NetworkMessage>) -> Slave<'a, T> {
		Slave {
			loader: loader,
			graph: None,
			channel: channel,
			bfs_kernel: None,
		}
	}

	pub fn on_message(&mut self, message: NetworkMessage) {
		match message {
			NetworkMessage::WorkerList(msg) => {
				let per_node = self.get_vertex_per_node(msg.addr.len() as ClientIndex);
				let start = per_node * msg.index as u64;
				let end = start + per_node;
				self.graph = Some(Rc::new(kernel1::load(&mut self.loader, start, end)));
			},
			NetworkMessage::InitKernel2(root) => {
				self.bfs_kernel = Some(BfsKernel::new(self.graph.as_ref().unwrap().clone()));

				if self.graph.as_ref().unwrap().is_valid(root) {
					self.bfs_kernel_process_node(root, None, 0, false);
				}
			},
			NetworkMessage::EdgeProcess(message) => {
				self.bfs_kernel_process_node(message.node, Some(message.parent), message.weight, false);
			},
			NetworkMessage::PerNodeQuery(size) => {
				let vertex = self.get_vertex_per_node(size);
				self.send_message(NetworkMessage::PerNodeQueryResult(vertex));
			},
			NetworkMessage::IsIdle(nonce) => {
				let traversed = self.bfs_kernel.as_ref().expect("bfs_kernel not initialized").traversed;
				self.send_message(NetworkMessage::NEdgeMessage(nonce, traversed));
			},
			_ => {}
		}
	}

	fn get_vertex_per_node(&self, nodes: ClientIndex) -> u64 {
		let graph_nodes = self.loader.config.as_ref().unwrap().n;
		let cluster_nodes = nodes;
		(graph_nodes as f64 / cluster_nodes as f64).ceil() as u64
	}

	fn bfs_kernel_process_node(&mut self, index: NodeIndex, parent: Option<NodeIndex>, weight: u64, child: bool) -> Option<VecDeque<(NodeIndex, Option<NodeIndex>, u64)>>{
		if !child {
			self.send_message(NetworkMessage::Working(true));
		}

		let result;

		{
			let mut bfs_kernel = self.bfs_kernel.as_mut().expect("bfs_kernel not initialized");
			result = bfs_kernel.process_node(index, parent,weight);
		}

		let mut local_queue = VecDeque::new();
		if let Some(result_list) = result {
			for item in result_list {
				if self.graph.as_ref().unwrap().is_valid(item.index) {
					local_queue.push_back((item.index, Some(item.parent), item.weight));
				} else {
					self.send_message(NetworkMessage::EdgeProcess(EdgeProcessMessage {
						node: item.index,
						parent: item.parent,
						weight: item.weight,
					}));
				}
			}
		}

		if child {
			Some(local_queue)
		} else {
			let mut index = 0;
			let mut count = local_queue.len();
			while let Some(item) = local_queue.pop_front() {
				index += 1;

				let segment = count / 10;
				if segment > 0 && index % segment == 0 {
					writeln!(&mut stderr(), "BFS> {}/{}", index, count).unwrap();
				}

				let mut child = self.bfs_kernel_process_node(item.0, item.1, item.2, true).unwrap();

				let child_len = child.len();
				count += child_len;
				if child_len > count / 10 {
					writeln!(&mut stderr(), "BFS> {}/{}", index, count).unwrap();
				}

				local_queue.append(&mut child);
			}

			None
		}
	}

	fn send_message(&mut self, message: NetworkMessage) {
		self.channel.send(message).unwrap()
	}
}
