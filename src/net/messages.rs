use std::net::SocketAddr;
use graph::NodeIndex;
use net::ClientIndex;

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub enum Addr{
	Remote(SocketAddr),
	Local(ClientIndex),
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct WorkerListMessage {
	pub addr: Vec<Addr>,
	pub index: ClientIndex,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct EdgeProcessMessage {
	pub parent: NodeIndex,
	pub node: NodeIndex,
	pub weight: u64,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum NetworkMessage {
	WorkerList(WorkerListMessage),
	Working(bool),

	InitKernel2(NodeIndex),
	EdgeProcess(EdgeProcessMessage),
	IsIdle(u32),
	NEdgeMessage(u32, u64),
	Stop,

	// master query messages (usually used for first slave only)
	PerNodeQuery(ClientIndex),
	PerNodeQueryResult(u64),
}
