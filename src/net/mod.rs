mod master;
mod slave;
mod slave_thread;
mod slave_net;
mod peekablereceiver;
pub mod messages;

pub use self::messages::NetworkMessage;
pub use self::master::Master;
pub use self::slave::Slave;
pub use self::slave_thread::fork as fork_slave;
pub use self::slave_net::SlaveNet;
pub use net::peekablereceiver::MakeIterPeekable;
use std::net::TcpStream;
use bincode;

type ClientIndex = u8;

fn read_packet(client: &mut TcpStream) -> bincode::serde::DeserializeResult<NetworkMessage> {
	bincode::serde::deserialize_from(client, bincode::SizeLimit::Infinite)
}
