use std::io::prelude::*;
use std::sync::mpsc::{Sender, Receiver, channel};
use std::thread;
use loader::Loader;
use net::messages::*;
use net::slave::Slave;
use net::peekablereceiver::MakeIterPeekable;

pub fn fork<U: Read + Seek + Send + 'static>(loader: Loader<U>) -> (Sender<NetworkMessage>, Receiver<NetworkMessage>, thread::JoinHandle<()>) {
	let (master_tx, slave_rx) = channel();
	let (slave_tx, master_rx) = channel();

	let handle = thread::Builder::new().name("local slave".to_string())
		.spawn(move|| {
			let mut ldr = loader;
			let mut slave = Slave::new(&mut ldr, slave_tx.clone());
			let mut last_working_state = false;

			let mut iter = slave_rx.iter_peekable();
			while let Some(message) = iter.next() {
				let has_next = iter.peek().is_some();

				match message {
					NetworkMessage::Stop => return,
					NetworkMessage::IsIdle(x) => {
						if !has_next {
							slave.on_message(NetworkMessage::IsIdle(x));
						}
					},
					message => {
						if !last_working_state {
							slave_tx.send(NetworkMessage::Working(true)).unwrap();
							last_working_state = true;
						}

						slave.on_message(message);

						if !has_next {
							slave_tx.send(NetworkMessage::Working(false)).unwrap();
							last_working_state = false;
						}
					},
				}
			}
			unreachable!("socket closed without sending exit message");
		}).unwrap();

	(master_tx, master_rx, handle)
}
