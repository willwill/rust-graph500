use std::net::TcpListener;
use std::time::Instant;
use std::sync::mpsc::{Sender, Receiver, channel};
use std::thread;
use std::io::Write;
use std::io::stderr;
use rand::StdRng;
use rand::Rng;
use bincode;
use duration_to_f64;
use net::ClientIndex;
use net::messages::*;
use net::read_packet;
use net::peekablereceiver::MakeIterPeekable;
use graph::NodeIndex;
use {print_mean_stats, print_stats};

type BusMessage = (NetworkMessage, Addr);

struct Client {
	pub addr: Addr,
	pub channel: Sender<NetworkMessage>,
}

pub struct Master{
	socket: TcpListener,
	size: ClientIndex,
	connected: Vec<Client>,
	node_count: NodeIndex,
	random: StdRng,
	// all slaves thread will write to this bus, the master only use receiver
	bus: (Sender<BusMessage>, Receiver<BusMessage>),
	local_threads: Vec<thread::JoinHandle<()>>,
	vertex_per_node: Option<u64>,
}

macro_rules! broadcast {
    ($this:ident, $message:expr) => {
		// println!(">> {:?}", message);
		for client in $this.connected.iter_mut() {
			client.channel.send($message.clone()).unwrap();
		}
    }
}

impl Master {
	pub fn new(addr: (&str, u16), size: ClientIndex, node_count: NodeIndex) -> Master {
		Master {
			socket: TcpListener::bind(addr).unwrap(),
			size: size,
			connected: Vec::with_capacity(size as usize),
			node_count: node_count,
			random: StdRng::new().unwrap(),
			bus: channel(),
			local_threads: Vec::new(),
			vertex_per_node: None,
		}
	}

	pub fn add_local_client(&mut self, slave: (Sender<NetworkMessage>, Receiver<NetworkMessage>)) {
		let index = self.connected.len() as ClientIndex;
		self.connected.push(Client{
			addr: Addr::Local(index),
			channel: slave.0,
		});
		self.monitor_rx(index, slave.1);
	}

	pub fn start(&mut self, times: usize) {
		self.connected[0].channel.send(NetworkMessage::PerNodeQuery(self.size)).unwrap();
		loop {
			match self.bus.1.recv() {
				Ok((NetworkMessage::PerNodeQueryResult(result), _)) => self.vertex_per_node = Some(result),
				Ok((NetworkMessage::Working(true), _)) => {},
				Ok((NetworkMessage::Working(false), _)) => break,
				_ => panic!("Queried PerNodeQuery, got unexpected result"),
			}
		}

		let mut now = Instant::now();
		self.wait_for_connections();
		println!("connection_time: {}", duration_to_f64(&now.elapsed()));

		now = Instant::now();
		self.kernel1();
		println!("construction_time: {}", duration_to_f64(&now.elapsed()));

		self.kernel2_loop(times);
		self.stop();
	}

	fn monitor_rx(&mut self, index: ClientIndex, rx: Receiver<NetworkMessage>) {
		let bus = self.bus.0.clone();
		self.local_threads.push(thread::Builder::new().name("local rx".to_string()).spawn(move || {
			loop {
				let packet = rx.recv();

				match packet {
					Ok(packet) => bus.send((packet, Addr::Local(index))).unwrap(),
					Err(_) => return,
				}
			}
		}).unwrap());
	}

	fn wait_for_connections(&mut self) {
		while (self.connected.len() as ClientIndex) < self.size {
			let (mut stream, addr) = self.socket.accept().unwrap();
			let bus = self.bus.0.clone();
			let (tx, rx) = channel();
			let mut sender_stream = stream.try_clone().unwrap();

			self.local_threads.push(thread::Builder::new().name(format!("peer {:?} recv", addr)).spawn(move || {
				loop {
					// got packet from slave, send to bus
					match read_packet(&mut stream) {
						Ok(packet) => bus.send((packet, Addr::Remote(addr.clone()))).unwrap(),
						_ => break
					}
				}
			}).unwrap());

			// we don't care to wait for recv thread to finish
			thread::Builder::new().name(format!("peer {:?} send", addr)).spawn(move || {
				loop {
					// got packet from master, send to slave
					match rx.recv().unwrap() {
						NetworkMessage::Stop => {
							bincode::serde::serialize_into(&mut sender_stream, &NetworkMessage::Stop, bincode::SizeLimit::Infinite).unwrap();
							return;
						}
						packet => bincode::serde::serialize_into(&mut sender_stream, &packet, bincode::SizeLimit::Infinite).unwrap(),
					}
				}
			}).unwrap();

			self.connected.push(Client {
				addr: Addr::Remote(addr),
				channel: tx,
			});
		}
	}

	fn stop(&mut self){
		broadcast!(self, NetworkMessage::Stop);

		for thread in self.local_threads.drain(..) {
			thread.join().unwrap();
		}
	}

	fn kernel1(&mut self) {
		// send each client their index
		let mut worker_list_message = WorkerListMessage {
			index: 0,
			addr: self.connected.iter().map(|item| item.addr.clone()).collect(),
		};

		for (index, client) in self.connected.iter_mut().enumerate() {
			worker_list_message.index = index as ClientIndex;
			let message = NetworkMessage::WorkerList(worker_list_message.clone());
			client.channel.send(message).unwrap();
		}

		// wait for all nodes to finish
		self.wait_for_ready();
	}

	// wait until all nodes are ready
	fn wait_for_ready(&mut self) {
		let mut ready = vec![false; self.size as usize];
		loop {
			let message = self.bus.1.recv().unwrap();
			let client_index = self.get_client_index(&message.1);
			match message.0 {
				NetworkMessage::Working(value) => {
					ready[client_index] = value;

					if ready.iter().all(|item| !item) {
						return;
					}
				},
				other => panic!("wait_for_ready: received unknown packet {:?}", other),
			}
		}
	}

	fn kernel2_loop(&mut self, times: usize) {
		let mut time_result = vec![0f64; times];
		let mut edge_result = vec![0f64; times];
		let mut teps = vec![0f64; times];

		let mut i = 0;
		while i < times {
			let now = Instant::now();

			let traversed = self.kernel2();
			if traversed == 0 {
				// ignore case that we picked empty root node
				continue
			}

			time_result[i] = duration_to_f64(&now.elapsed());
			edge_result[i] = traversed as f64;
			teps[i] = edge_result[i] / time_result[i];

			i += 1;
		}

		// time result
		print_stats("time", &time_result);
		print_mean_stats("time", &time_result);

		// nedge result
		print_stats("nedge", &edge_result);
		print_mean_stats("nedge", &edge_result);

		// teps
		print_stats("TEPS", &teps);

		let mut harmonic_mean: f64 = teps.iter().map(|x| 1f64/x).sum();
		harmonic_mean = times as f64 / harmonic_mean;
		let inv_mean = 1f64/harmonic_mean;

		let mut harmonic_stdev: f64 = teps.iter().map(|x| (1f64/x - inv_mean).powi(2)).sum();
		harmonic_stdev = (harmonic_stdev / (times as f64-1f64)).sqrt() / ((times as f64-1f64) * harmonic_mean.powi(2));

		println!("harmonic_mean_TEPS: {}", harmonic_mean);
		println!("harmonic_stddev_TEPS: {}", harmonic_stdev);
	}

	fn get_client_index(&self, addr: &Addr) -> usize {
		self.connected.iter().position(|item| item.addr == *addr).expect("unknown client")
	}

	fn kernel2(&mut self) -> u64{
		writeln!(&mut stderr(), "kernel2 run").unwrap();
		let root = self.random.gen_range(0, self.node_count);
		broadcast!(self, NetworkMessage::InitKernel2(root));
		let mut working = vec![true; self.size as usize];
		let mut nedges: Vec<Option<u64>> = vec![None; self.size as usize];
		let vertex_per_node = self.vertex_per_node.unwrap();
		let mut idle_nonce = 0u32;

		let mut iter = self.bus.1.iter_peekable();

		while let Some(message) = iter.next() {
			// println!("<< {:?}", message);
			let client_index = self.get_client_index(&message.1);
			writeln!(&mut stderr(), "<< {:?}", message).unwrap();
			match message.0 {
				NetworkMessage::EdgeProcess(ref edge_process) => {
					// reroute message to the destination
					let node_index = (edge_process.node / vertex_per_node) as usize;
					working[node_index] = true;
					nedges[client_index] = None;
					self.connected[node_index].channel.send(message.0.clone()).unwrap();
				},
				NetworkMessage::Working(value) => {
					working[client_index] = value;
					nedges[client_index] = None;

					if working.iter().all(|item| !item) && iter.peek().is_none() {
						idle_nonce += 1;
						broadcast!(self, NetworkMessage::IsIdle(idle_nonce));
					}
				},
				NetworkMessage::NEdgeMessage(nonce, value) => {
					if nonce != idle_nonce {
						continue;
					}
					nedges[client_index] = Some(value);

					writeln!(&mut stderr(), "{:?}", nedges).unwrap();
					if nedges.iter().all(|item| item.is_some()) && iter.peek().is_none() {
						break;
					}
				},
				_ => panic!("kernel2: received unknown packet"),
			}
		}

		nedges.iter().map(|item| item.unwrap()).sum()
	}
}
