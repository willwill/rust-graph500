#![feature(proc_macro)]
#![feature(test)]

extern crate test;
extern crate rand;
#[macro_use]
extern crate serde_derive;
extern crate bincode;

mod config;
mod graph;
mod generator;
pub mod kernel1;
// pub mod sssp;
mod loader;
mod bfs;
pub mod net;

pub use config::Config;
pub use generator::kronecker_generator;
pub use generator::Edge as GeneratorEdge;
pub use loader::*;
pub use graph::*;
pub use bfs::*;

use test::stats::Stats;
use std::time::Duration;

pub fn duration_to_f64(duration: &Duration) -> f64 {
	duration.as_secs() as f64 + (duration.subsec_nanos() as f64 / 1e9f64)
}

pub fn print_stats(name: &str, data: &[f64]) {
	println!("min_{}: {}", name, data.min());
	let quartiles = data.quartiles();
	println!("firstquartile_{}: {}", name, quartiles.0);
	println!("median_{}: {}", name, quartiles.1);
	println!("thirdquartile_{}: {}", name, quartiles.2);
	println!("max_{}: {}", name, data.max());
}

pub fn print_mean_stats(name: &str, data: &[f64]) {
	println!("mean_{}: {}", name, data.mean());
	println!("stddev_{}: {}", name, data.std_dev());
}
