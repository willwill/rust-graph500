extern crate clap;
extern crate graph500;

use std::io::BufReader;
use std::fs::File;
use std::thread::sleep;
use std::time::Duration;
use clap::{App, Arg};
use graph500::*;

fn build_loader(filename: &str) -> Loader<BufReader<File>> {
	let file = File::open(filename).unwrap();
	let buf_file = BufReader::new(file);
	Loader::new(buf_file)
}

fn main(){
	let cli = App::new("Graph500 BFS")
		.arg(Arg::with_name("input")
			.help("Input file name")
			.index(1)
			.required(true))
		.arg(Arg::with_name("cluster-size")
			.long("master")
			.help("Run in master mode. Cluster size (incl. self) required")
			.takes_value(true))
		.arg(Arg::with_name("bind")
			.help("Interface to listen")
			.long("bind")
			.short("b")
			.default_value("0.0.0.0")
			.takes_value(true))
		.arg(Arg::with_name("port")
			.help("TCP port to connect/listen")
			.long("port")
			.short("p")
			.default_value("11123")
			.takes_value(true))
		.arg(Arg::with_name("master-ip")
			.long("slave")
			.help("Run in slave mode. Master IP required")
			.takes_value(true))
		.arg(Arg::with_name("runs")
			.short("r")
			.long("runs")
			.default_value("64")
			.help("Times to run kernel 2. Only affect master mode")
			.takes_value(true))
		.arg(Arg::with_name("slave-count")
			.long("slave-count")
			.default_value("1")
			.help("Number of local slave in master mode")
			.takes_value(true))
		.get_matches();

	let filename = cli.value_of("input").unwrap();
	let loader = build_loader(filename);

	let node_count;
	{
		let config = loader.config.as_ref().unwrap();
		config.print();
		node_count = config.n;
	}

	let port = cli.value_of("port").unwrap().parse().unwrap();

	if let Some(cluster_size) = cli.value_of("cluster-size") {
		let runs = cli.value_of("runs").unwrap().parse().unwrap();
		println!("NBFS: {}", runs);

		let bind = cli.value_of("bind").unwrap();
		let mut master = net::Master::new(
			(bind, port),
			cluster_size.parse().unwrap(),
			node_count,
		);

		let slave_count = cli.value_of("slave-count").unwrap().parse().unwrap();
		let mut slave_threads = Vec::with_capacity(slave_count);
		if slave_count > 0 {
			let slave = net::fork_slave(loader);
			master.add_local_client((slave.0, slave.1));
			slave_threads.push(slave.2);
		}
		// Note: skip the first item by starting at 1
		for _ in 1..slave_count {
			let slave_loader = build_loader(filename);
			let slave = net::fork_slave(slave_loader);
			master.add_local_client((slave.0, slave.1));
			slave_threads.push(slave.2);
		}

		master.start(runs);

		for item in slave_threads {
			item.join().unwrap();
		}
	} else if let Some(master_ip) = cli.value_of("master-ip") {
		let slave = net::fork_slave(loader);
		let mut slave_net = None;

		for _ in 0..100 {
			match net::SlaveNet::new((master_ip, port)) {
				Ok(net) => {
					slave_net = Some(net);
					break;
				},
				_ => {
					sleep(Duration::from_millis(100));
				}
			}
		}

		slave_net.unwrap().bind_slave((slave.0, slave.1));
		slave.2.join().unwrap();
	}
}
