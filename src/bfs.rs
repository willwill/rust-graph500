use std::rc::Rc;
use graph::*;

#[derive(Clone)]
pub struct VertexInfo{
	distance: Option<u64>,
	parent: Option<NodeIndex>
}

pub struct BfsKernel {
	graph: Rc<Graph<Node>>,
	pub vertex_info: Graph<VertexInfo>,
	pub traversed: u64,
}

pub struct ProcessNodeResult {
	pub index: NodeIndex,
	pub parent: NodeIndex,
	pub weight: u64,
}

impl BfsKernel {
	pub fn new(graph: Rc<Graph<Node>>) -> BfsKernel {
		let vertex_info = Graph::new_from_graph(&graph, VertexInfo{
			distance: None,
			parent: None,
		});

		BfsKernel {
			graph: graph,
			vertex_info: vertex_info,
			traversed: 0,
		}
	}

	pub fn process_node(&mut self, index: NodeIndex, parent: Option<NodeIndex>, weight: u64) -> Option<Vec<ProcessNodeResult>> {
		let ref mut info = self.vertex_info[index];

		if info.distance == None || weight < info.distance.unwrap() {
			info.distance = Some(weight);
			info.parent = parent;

			self.traversed += self.graph[index].edges.len() as u64;

			return Some(self.graph[index].edges.iter().map(|edge| {
				ProcessNodeResult {
					index: edge.other,
					parent: index,
					weight: edge.weight.parse() + weight,
				}
			}).collect());
		}

		None
	}
}
