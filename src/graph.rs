use std::ops::Index;
use std::ops::IndexMut;
use std::default::Default;

pub type NodeIndex = u64;

#[derive(Clone, Debug)]
pub struct ZST{}
impl ZST{
    pub fn parse(&self) -> u64 { 1u64 }
}
impl Default for ZST {
    fn default() -> ZST {ZST {}}
}

pub type Weight = ZST;

#[derive(Clone, Debug)]
pub struct Edge {
    pub other: NodeIndex,
    pub weight: Weight,
}

impl Edge {
    pub fn new(index: NodeIndex, weight: Weight) -> Edge {
        Edge {
            other: index,
            weight: weight,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Node {
    pub edges: Vec<Edge>,
}

impl Node {
    pub fn new() -> Node {
        Node {
            edges: Vec::new(),
        }
    }

    pub fn edge_to(&self, other: NodeIndex) -> Option<&Edge> {
        for edge in &self.edges {
            if edge.other == other {
                return Some(edge);
            }
        }
        None
    }
}

pub struct Graph<T> {
    pub nodes: Vec<T>,
    pub start_index: u64,
}

impl<T> Graph<T> {
    pub fn is_valid(&self, index: NodeIndex) -> bool {
        index >= self.start_index && index - self.start_index < self.nodes.len() as u64
    }
}

impl<T: Clone> Graph<T> {
    pub fn new(template: T, size: usize, start_index: u64) -> Graph<T> {
        Graph{
            nodes: vec![template; size],
            start_index: start_index,
        }
    }

    pub fn new_from_graph<U>(graph: &Graph<U>, template: T) -> Graph<T> {
        Graph{
            nodes: vec![template; graph.nodes.len()],
            start_index: graph.start_index,
        }
    }
}

impl Graph<Node> {
    pub fn add_edge(&mut self, from: NodeIndex, to: NodeIndex, weight: Weight){
        let edge = Edge::new(to, weight);
        self[from].edges.push(edge);
    }
}

impl<T> Index<NodeIndex> for Graph<T> {
    type Output = T;

    fn index(&self, index: NodeIndex) -> &T {
        &self.nodes[(index - self.start_index) as usize]
    }
}

impl<T> IndexMut<NodeIndex> for Graph<T> {
    fn index_mut(&mut self, index: NodeIndex) -> &mut T {
        &mut self.nodes[(index - self.start_index) as usize]
    }
}
