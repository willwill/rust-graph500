use petgraph::graph::NodeIndex;
use petgraph::visit::EdgeRef;
use Graph;

#[derive(Clone)]
pub struct VertexInfo{
	distance: Option<u64>,
	parent: Option<NodeIndex<usize>>
}

pub struct SsspResult {
	pub result: Vec<VertexInfo>,
	pub edge_traversed: u64,
}

pub struct SsspKernel<'a> {
	graph: &'a Graph,
}

impl<'a> SsspKernel<'a> {
	pub fn new(graph: &Graph) -> SsspKernel {
		SsspKernel {
			graph: graph,
		}
	}

	pub fn run(&self, root: &'a NodeIndex<usize>) -> SsspResult {
		let mut node_info = vec![VertexInfo{ distance: None, parent: None }; self.graph.node_count()];
		node_info[root.index()].distance = Some(0);

		let mut nodes: Vec<NodeIndex<usize>> = self.graph.node_indices().collect();
		let mut edge_traversed = 0u64;

		while !nodes.is_empty() {
			// index in the nodes vector
			let nodes_index;
			// the NodeIndex
			let index;
			{
				let result = nodes.iter().enumerate().min_by_key(|item| {
					node_info[item.1.index()].distance.unwrap_or(u64::max_value())
				}).unwrap();
				nodes_index = result.0.clone();
				index = result.1.clone();
			}
			nodes.remove(nodes_index);

			let distance = match node_info[index.index()].distance {
				Some(x) => x,
				None => break, // the graph is not fully connected
			};

			for edge in self.graph.edges(index.clone()) {
				edge_traversed += 1;
				let weight = distance + *edge.weight() as u64;
				let current_distance = node_info[edge.target().index()].distance;
				let should_update = match current_distance {
					Some(val) => weight < val,
					None => true
				};
				if should_update {
					node_info[edge.target().index()].distance = Some(weight);
					node_info[edge.target().index()].parent = Some(index.clone());
				}
			}
		}

		SsspResult{
			result: node_info,
			edge_traversed: edge_traversed,
		}
	}
}
